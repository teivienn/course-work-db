import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/', //  /
      name: 'LoginPageComponent',
      component: require('@/components/LoginPageComponent').default
    },
    {
      path: '/reg',
      name: 'RegPageComponent',
      component: require('@/components/RegPageComponent').default
    },
    {
      path: '/home', //home
      name: 'HomePageComponent',
      component: require('@/components/HomePage/HomePageComponent').default
    },
    {
      path: '/real',
      name: 'RealPageComponent',
      component: require('@/components/HomePage/RealPageComponent').default
    },
    {
      path: '/paint',
      name: 'PaintPageComponent',
      component: require('@/components/HomePage/PaintPageComponent').default
    },
    {
      path: '/write',
      name: 'WritePageComponent',
      component: require('@/components/HomePage/WritePageComponent').default
    },
    {
      path: '/photo',
      name: 'PhotoPageComponent',
      component: require('@/components/HomePage/PhotoPageComponent').default
    },
    {
      path: '/loading',
      name: 'LoadingComponent',
      component: require('@/components/LoadingComponent/LoadingComponent').default
    },
    {
      path: '/add',
      name: 'AddExponatComponent',
      component: require('@/components/AddInformationComponent/AddExponatComponent').default
    },
    {
      path: '/addauthor',
      name: 'AddAuthorsComponent',
      component: require('@/components/AddInformationComponent/AddAuthorsComponent').default
    },
    {
      path: '/help',
      name: 'HelpPageComponent',
      component: require('@/components/HomePage/HelpPageComponent').default
    },
    {
      path: '/wiev/:id',
      name: 'WievPageComponent',
      component: require('@/components/HomePage/WievPageComponent').default,
      props: true,
    },
    {
      path: '/all',
      name: 'AllPageComponent',
      component: require('@/components/HomePage/AllPageComponent').default,
    }
  ]
})
