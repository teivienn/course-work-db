import { db } from './bd';
import moment from 'moment'


export async function getUser(){
    let result;
    console.log("sdsdsd", db);
    await db.any('select * from workers')
    .then(data =>{
        result = data;        
    })

    return result;
}


export async function Login(login, password){
    // console.log('login', login);
    // console.log('password', password);
    
    let result;
    await db.any('select * from workers where w_login = $1 or w_password = $2', [login, password])
    .then(data => {
        result = data;
        // console.log(data);
        
    })
    .catch(error =>{
        result = error;   
    })

    return result;
};


export async function checkLogin(login){
    let result;
    await db.any('select * from workers where w_login = $1', login)
    .then(data => {
        result = data;
        
    })
    .catch(error =>{
        // result = error;   
        console.log("error", error);
    })

    return result;
}

export async function getUserById(id){
    let result;
    await db.any('select * from workers where w_id = $1', id)
    .then(data => {
        result = data;
        
    })
    .catch(error =>{
        // result = error;   
        console.log("error", error);
    })

    return result;
}


export async function Registration(w_name, w_surname, w_dateofbidth, login, password){
    let result;
    await db.none('insert into workers(w_name, w_surname, w_dateofbirth, W_login, w_password) values($1, $2, $3, $4, $5)', [w_name, w_surname, w_dateofbidth, login, password])
    .then(() => {
        result = 'ok';
    })
    .catch(error => {
        console.log("error", error);
        
    })
    return result;
}


export async function getCategoties(){
    let result;
    await db.any('select * from categories')
    .then(data => {
        result = data;
    })
    .catch(error => {
        console.log("error", error);
    })
    return result;
}

export async function getAuthors(){
    let result;
    await db.any('select * from authors')
    .then(data => {
        result = data;
    })
    .catch(error => {
        console.log("error", error);
    })
    return result;
}

export async function addAuthors(image, name, surname, dateofbidth, dateofdead, description){
    let result;
    await db.none('insert into authors(a_image, a_name, a_surname, a_dateofbidth, a_dateofdead, a_description) values($1, $2, $3, $4, $5, $6)', [image, name, surname, dateofbidth, dateofdead, description])
    .then(() => {
        result = "ok";
    })
    .catch(error => {
        console.log("error", error);
    })
    return result;
}

export async function addExpo(g_e_id, g_e_name, g_e_year, author_e_id, g_e_image, category_e_id, g_e_safety, i_e_recordingdate, workers_e_id, i_e_fromwhom, i_e_price, g_e_description){
    let result;
    await db.one('select addexpo($1, $2, $3, $4, $5, $6, $7, $8, $9 ,$10 ,$11, $12)', [g_e_id, g_e_name, g_e_year, author_e_id, g_e_image, category_e_id, g_e_safety, i_e_recordingdate, workers_e_id, i_e_fromwhom, i_e_price, g_e_description])
    .then(data => {
        console.log(data);
        
        result = "ok";
    })
    .catch(error => {
        console.log("error", error);
    })
    return result;
}

export async function getGalery(category){
    let result;
    await db.any('select * from galery where category_id = $1', category)
    .then(data => {
        for(let i = 0; i < data.length; i++){
            data[i]['g_year'] = transformationsDate(data[i]['g_year']);
        }
        
        result = data;
    })
    .catch(error => {
        console.log("error", error);
    })
    return result;
}

export async function getGaleryall(){
    let result;
    await db.any('select * from galery left join authors a on galery.author_id = a.a_id left join categories c on galery.category_id = c.c_id left join inventory i on galery.g_id = i.galery_id left join workers w on i.workers_id = w.w_id')
    .then(data => {
        for(let i = 0; i < data.length; i++){
            data[i]['g_year'] = transformationsDate(data[i]['g_year']);
            data[i]['i_recordingdate'] = transformationsDate(data[i]['i_recordingdate']);
        }
        
        result = data;
    })
    .catch(error => {
        console.log("error", error);
    })
    return result;
}

export async function search(name){
    let result;
    await db.any('select * from galery left join authors a on galery.author_id = a.a_id left join categories c on galery.category_id = c.c_id left join inventory i on galery.g_id = i.galery_id left join workers w on i.workers_id = w.w_id where g_name like \'%$1#%\'', name)
    .then(data => {
        for(let i = 0; i < data.length; i++){
            data[i]['g_year'] = transformationsDate(data[i]['g_year']);
            data[i]['i_recordingdate'] = transformationsDate(data[i]['i_recordingdate']);
        }
        
        result = data;
    })
    .catch(error => {
        console.log("error", error);
    })
    return result;
}


export async function searchdate(date1, date2){
    let result;
    await db.any('select * from galery left join authors a on galery.author_id = a.a_id left join categories c on galery.category_id = c.c_id left join inventory i on galery.g_id = i.galery_id left join workers w on i.workers_id = w.w_id where g_year BETWEEN  $1 AND $2', [date1, date2])
    .then(data => {
        for(let i = 0; i < data.length; i++){
            data[i]['g_year'] = transformationsDate(data[i]['g_year']);
            data[i]['i_recordingdate'] = transformationsDate(data[i]['i_recordingdate']);
        }
        
        result = data;
    })
    .catch(error => {
        console.log("error", error);
    })
    return result;
}


export async function searchcategory(cat){    
    let result;
    await db.any('select * from galery left join authors a on galery.author_id = a.a_id left join categories c on galery.category_id = c.c_id left join inventory i on galery.g_id = i.galery_id left join workers w on i.workers_id = w.w_id where c_id in ($1:csv)', [cat])
    .then(data => {
        for(let i = 0; i < data.length; i++){
            data[i]['g_year'] = transformationsDate(data[i]['g_year']);
            data[i]['i_recordingdate'] = transformationsDate(data[i]['i_recordingdate']);
        }
        
        result = data;
    })
    .catch(error => {
        console.log("error", error);
    })
    return result;
}


export async function getWievGalery(id){
    let result;
    await db.one('select * from galery left join authors a on galery.author_id = a.a_id left join categories c on galery.category_id = c.c_id left join inventory i on galery.g_id = i.galery_id left join workers w on i.workers_id = w.w_id where g_id = $1', id)
    .then(data => {
        data.g_year = transformationsDate(data.g_year);
        data.i_recordingdate = transformationsDate(data.i_recordingdate);

        result = data;
    })
    .catch(error => {
        console.log("error", error);
    })
    return result;
}


function transformationsDate(date){
    // console.log(date);
    date = moment(date).format('YYYY-MM-DD');
    // console.log(date);
    
    return date;
};
